const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI";

// JSON Web Tokens

	// Token Creation
	module.exports.createAccessToken = (user) =>
	{
		
		// Modify the payload
		const data =
		{
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		}

		// Generate a token 
		return jwt.sign(data, secret, {})
	};


	//Token Verification
	module.exports.verify = (req, res, next) => 
	{
		let token = req.headers.authorization

		if (typeof token !== "undefined" )
		{
			token = token.slice(7, token.length);
			
			return jwt.verify(token, secret, (err,data) =>
			{
				if (err)
				{
					return res.send({auth:"failed"})
				}
				else 
				{	next()}
			})
		}

		else 
		{
			return res.send({auth: "failed"});
		}
	};


	// Token Decryption
	module.exports.decode = (token) =>
	{
		if (typeof token !== "undefined")
		{
			token = token.slice(7, token.length);

			return jwt.verify (token, secret, (err,data) =>
			{
				if (err)
				{	return null	}
				else 
				{
					return jwt.decode(token, {complete:true}).payload
				}
			})
		}
		else
		{
			return null
		}
	};


