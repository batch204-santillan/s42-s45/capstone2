// CONNECT TO MODEL CONTENTS & FILES
	const User = require("../models/User");
	const Product = require("../models/Product");
	const Cart = require("../models/Cart");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");

	//  ADD TO CART
	module.exports.addToCart = async (data) =>
	{
		if (data.isAdmin === false)
		{	
			let prodDetails = await Product.findById(data.productId).then(result => {return result}) 
				//console.log (prodDetails);

			let userDetails = await User.findById(data.userId).then(result => {return result}) 
				//console.log (userDetails); 

			let updateCart = await Cart.findOne({email: data.email}).then(cart =>
			{
				cart.products.push(
				{
					productId: data.productId,
					productName: prodDetails.name,
					quantity: data.quantity,
					price: prodDetails.price,
					subtotal: data.quantity * prodDetails.price
				})	
				
				return cart.save().then((cart, error) =>
				{
					if (error)
						{	return false}
					else
						{	return	true}
				})
			});


			if (updateCart)
				{	/*userDetails = await User.findById(data.userId).then(result => {return result})
					console.log (userDetails);*/

					return 'Added to cart!'
				}
			else
				{	return false}
		}

		else
		{
			return "Only customers can add to cart!"
		}
	};


	// CHANGE QUANTITY *not yet done
	module.exports.changeQuantity = (data) =>
	{
		if (data.isAdmin === false)
		{
			
			return Cart.updateOne({_id: data.cartId},{$set:{products: {quantity: data.quantity}}}).then((result, error) =>
			{
				if (error)
					{	return false}
				else
					{	console.log (result)
						return "quantity updated!"}
			})
		}
		else
			{	return false}
	};
	
	// SUBTOTAL FOR EACH ITEM


	// TOTAL PRICE FOR ALL 