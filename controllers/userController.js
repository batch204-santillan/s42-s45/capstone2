// CONNECT TO MODEL CONTENTS & FILES
	const User = require("../models/User");
	const Product = require("../models/Product");
	const Cart = require("../models/Cart");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");

// CONTROLLERS
		
	// Controller for Checking if email exists
		module.exports.checkEmailExists = (reqBody) =>
		{
			return User.find({email: reqBody.email}) .then(result =>
			{
				if (result.length > 0)
					{ return true }
				else
					{ return false }
			})
		}

	// USER REGISTRATION
	module.exports.registerUser = (reqBody) =>
	{
		let newUser = new User (
		{
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password,10)
		})

		return newUser.save().then((user,error) =>
		{
			if (error)
				{return false}
			else
				{return true}
		})
	};

	// CART CREATION
	module.exports.registerCart =  (reqBody) =>
	{
		let newCart = new Cart (
				{
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email: reqBody.email
				})

		return newCart.save().then((cart, error) =>
		{
			if (error)
				{	return false}
			else
				{	return true}
		})
	};


	// USER LOGIN / AUTHENTICATION
	module.exports.loginUser = (reqBody) =>
	{
		return User.findOne({email: reqBody.email}).then (result =>
		{
			if (result == null)
				{	return false}
			else
			{
				const passwordCheck = bcrypt.compareSync (reqBody.password , result.password);

				if (passwordCheck === true)
					{	console.log(result)
						return{access: auth.createAccessToken(result)}
					}
				else
					{	return false}
			}
		})
	}

	// RETRIEVE USER DETAILS
	module.exports.getUserDetails = (data) =>
	{
		return User.findById(data.id).then(result =>
		{
			result.password = "hidden"
			return result
		})
	};


	// USER CHECKOUT (CREATE ORDER MULTIPLE ITEMS )
	module.exports.checkOut = async (data, productData) =>
	{
		if (data.isAdmin === false)
		{	
			
			let userDetails = await User.findById(data.userId).then(result => {return result}) 
				//console.log (userDetails); 

			let updateUser = await User.findById(data.userId).then(user =>
			{
				user.orders.push(
				{
					products: productData.products,
					totalAmount: productData.totalAmount
				})
				//console.log (productData)
				return user.save().then((user, error) =>
				{
					if (error)
						{	return false}
					else
						{	return	true}
				})
			});

			if (updateUser)
				{	/*userDetails = await User.findById(data.userId).then(result => {return result})
					console.log (userDetails);*/

					return true
				}
			else
				{	return false}
		}

		else
		{
			return false
		}
	};

	// USER CHECKOUT (CREATE SINGLE ORDER) 
	module.exports.checkOut1 = async (data) =>
	{
		if (data.isAdmin === false)
		{	
			let prodDetails = await Product.findById(data.productId).then(result => {return result}) 
				//console.log (prodDetails);

			let userDetails = await User.findById(data.userId).then(result => {return result}) 
				//console.log (userDetails); 

			let updateUser = await User.findById(data.userId).then(user =>
			{
				user.orders.push(
				{
					products: {
						productId: data.productId,
						productName: prodDetails.name,
						quantity: data.quantity,
						price: prodDetails.price
					},
					totalAmount: data.quantity * prodDetails.price
				})
				return user.save().then((user, error) =>
				{
					if (error)
						{	return false}
					else
						{	return	true}
				})
			});

			let updateProduct = await Product.findById(data.productId).then(product =>
			{				
				product.orders.push(
					{
						nameOfCustomer: userDetails.firstName + " " +userDetails.lastName,
						email: userDetails.email,
						quantity: data.quantity,
						totalAmount: data.quantity * prodDetails.price,
						placedOn: new Date()
					})
				return product.save().then((prod, error) =>
					{
						if (error)
							{	return false}
						else
							{	return true}
					})
			});

			if (updateUser && updateProduct)
				{	/*userDetails = await User.findById(data.userId).then(result => {return result})
					console.log (userDetails);*/

					return 'Order Successful!'
				}
			else
				{	return false}
		}

		else
		{
			return "Only customers can place an order"
		}
	};


	// SET USER AS ADMIN
	module.exports.makeAdmin = (reqBody, isAdmin) => 
	{
		if (isAdmin)
		{
			let user =
			{
				isAdmin: true
			};

			return User.findByIdAndUpdate(reqBody.userId, user).then((user, error) =>
			{
				if (error)
					{	return false}
				else
					{	return "User is now admin"}
			})
		}
		else
			{	return false}
	};


	// RETRIEVE AUTHENTICATED USER'S ORDERS
	module.exports.getOrders = (data) =>
	{
		return User.findById(data.id).then(result =>
		{
			return result.orders
		})
	};

	// RETRIEVE ALL ORDERS (ADMIN ONLY)
	module.exports.getAllOrders = (data) =>
	{
		if (data.isAdmin)
		{
			const pipeline = [
			{
				"$match" : {"isAdmin": false}
			},
			{
				"$project" : 
					{	"email" : 1,
						"orders" : 1
					}
			}
			]

			return User.aggregate(pipeline).then(result =>
					{return result}
				)
		}
		else
			{	return false}
	};


	
	
