// CONNECT TO MODEL CONTENTS & FILES
	const User = require("../models/User");
	const Product = require("../models/Product");
	const Cart = require("../models/Cart");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");

// CONTROLLERS
	// CREATE A PRODUCT (ADMIN)
	module.exports.createProduct = (data, reqBody) =>
	{
		if (data.isAdmin === true)
		{
			let newProduct = new Product (
			{
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				image: reqBody.image
			});

			return newProduct.save().then((product, error) =>
			{
				if (error)
					{	return false}
				else
					{	return true}
			});
		}
		else
			{	return false}
	};

	// RETRIEVE ALL PRODUCTS
		module.exports.getAllProducts =() =>
		{
			return Product.find({}).then (result =>
			{
				return result;
			})
		}


	//RETRIEVE ACTIVE PRODUCTS

	module.exports.showActiveProducts = () =>
	{
		const pipeline = [
			{
				"$match" : {"isActive": true}
			},
			{
				"$project" : 
					{	"name" : 1,
						"description" : 1,
						"price" :1
					}
			}
			]

		return Product.aggregate(pipeline).then(result =>
			{return result}
				)
	};


	// RETRIEVE A SINGLE PRODUCT
	module.exports.getProduct = (reqParams) =>
	{
		return Product.findById(reqParams.productId).then(result =>
		{
			return result;
		})
	};

	// UPDATE PRODUCT INFO (ADMIN)
	module.exports.updateProduct = (reqParams, data, reqBody) =>
	{
		return Product.findById(data.id).then(result =>
		{
			if (data.isAdmin === true)
			{
				let updatedProduct = {
					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price,
					image: reqBody.image
				}

				return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) =>
				{
					if (error)
						{	return false}
					else
						{	return true}
				})
			}
			else
				{	return false}
		})
		
	};

	// ARCHIVE A PRODUCT (ADMIN)
	module.exports.archiveProduct = (reqParams, reqBody, isAdmin) =>
	{
		if (isAdmin)
		{
			let archivedProduct =
			{
				isActive: reqBody.isActive
			};

			return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) =>
			{
				if (error)
					{	return false}
				else
					{	return true}
			})
		}
		else
			{	return false}
	};

	// ACTIVATE A PRODUCT (ADMIN)
	module.exports.activateProduct = (reqParams, isAdmin) =>
	{
		if (isAdmin)
		{
			let activeProduct =
			{
				isActive: true
			};

			return Product.findByIdAndUpdate(reqParams.productId, activeProduct).then((product, error) =>
			{
				if (error)
					{	return false}
				else
					{	return true}
			})
		}
		else
			{	return false}
	};







