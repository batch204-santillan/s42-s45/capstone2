// LOAD DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const auth = require("../auth");
	const User = require("../models/User");
	const Product = require("../models/Product");
	const Cart = require("../models/Cart");

//	LOAD userController
const cartController = require("../controllers/cartController");

// ADD TO CART
router.post("/addToCart" , auth.verify, (req, res) =>
{
	let data =
	{
		productId: req.body.productId,
		quantity: req.body.quantity,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		email: auth.decode(req.headers.authorization).email
	}

	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});



// CHANGE QUANTITY
	router.put("/update", auth.verify, (req, res) =>
	{
		let data =
		{
			cartId: req.body.cartId,
			quantity: req.body.newQuantity,
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		cartController.changeQuantity(data).then(resultFromController => res.send(resultFromController));
	});


	// SUBTOTAL FOR EACH ITEM


	// TOTAL PRICE FOR ALL 



/*		------------- ALWAYS AT BOTTOM -------------		*/
// EXPORT MODULES
	module.exports = router;
	