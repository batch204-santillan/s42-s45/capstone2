// LOAD DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const auth = require("../auth");

//	LOAD productController
const productController = require("../controllers/productController");

// ROUTES
	// CREATE A PRODUCT (ADMIN)
	router.post("/create",  auth.verify, (req, res) =>
	{
		const userData = 
		{ isAdmin: auth.decode(req.headers.authorization).isAdmin }

		productController.createProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
	});

	// ROUTE FOR RETRIEVING ALL PRODUCTS
		router.get ("/all", (req, res) =>
		{
			productController.getAllProducts().then(resultFromController => res.send(resultFromController));
		});

	//	RETRIEVE ACTIVE PRODUCTS
	router.get("/browseProducts", auth.verify, (req,res) =>
	{
		productController.showActiveProducts().then(resultFromController => res.send(resultFromController));
	});

	// RETRIEVE A SINGLE PRODUCT
	router.get ("/:productId" , (req, res) =>
	{
		productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
	});

	// UPDATE PRODUCT INFO (ADMIN)
	router.put("/update/:productId", auth.verify, (req, res) =>
	{
		const userData = auth.decode(req.headers.authorization) 

		productController.updateProduct(req.params, userData, req.body).then(resultFromController => res.send(resultFromController));
	});

	// ARCHIVE A PRODUCT (ADMIN)
	router.put ("/:productId/archive",  auth.verify, (req, res) =>
	{
		const isAdmin = auth.decode(req.headers.authorization).isAdmin

		productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
	});

	// ACTIVATE A PRODUCT (ADMIN)
	router.put ("/:productId/activate",  auth.verify, (req, res) =>
	{
		const isAdmin = auth.decode(req.headers.authorization).isAdmin

		productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	});





/*		------------- ALWAYS AT BOTTOM -------------		*/
// EXPORT MODULES
	module.exports = router; 
	