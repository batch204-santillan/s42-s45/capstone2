// LOAD DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const auth = require("../auth");
	const User = require("../models/User");
	const Product = require("../models/Product");
	const Cart = require("../models/Cart");

//	LOAD userController
const userController = require("../controllers/userController");

// ROUTES

	// ROUTE FOR CHECKING: if the user's email already exists in our database
	router.post("/checkEmail", (req, res) =>
	{
		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
	});


	// USER REGISTRATION
	router.post("/register", (req, res) =>
	{
		let userRegistered = userController.registerUser(req.body).then(resultFromController => {return resultFromController});
		let cartRegistered = userController.registerCart(req.body).then(resultFromController => {return resultFromController});
		if (userRegistered && cartRegistered)
		{
			res.send (true)
		}
		else
			{	res.send (false)}
	});
	

	// USER LOGIN / AUTHENTICATION
	router.post("/login", (req, res) =>
	{
		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
	});

	// RETRIEVE USER DETAILS
	router.get("/details" , auth.verify, (req, res) =>
	{	
		const userData = auth.decode(req.headers.authorization)

		userController.getUserDetails({id: userData.id}).then(resultFromController => res.send(resultFromController));
	});


	// USER CHECKOUT (CREATE ORDER MULTIPLE ITEMS )
	router.post("/order" , auth.verify, (req, res) =>
	{
		let data =
		{
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		let productData =
		{
			products: req.body.products,
			totalAmount: req.body.totalAmount
		}

		userController.checkOut(data, productData).then(resultFromController => res.send(resultFromController));
	});


	// USER CHECKOUT (CREATE ORDER SINGLE ORDER ) 
	router.post("/order1" , auth.verify, (req, res) =>
	{
		let data =
		{
			productId: req.body.productId,
			quantity: req.body.quantity,
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		userController.checkOut1(data).then(resultFromController => res.send(resultFromController));
	});


	// SET USER AS ADMIN
	router.put("/makeAdmin", auth.verify, (req, res) =>
	{	
		const isAdmin = auth.decode(req.headers.authorization).isAdmin

		userController.makeAdmin(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
	});

	
	// RETRIEVE AUTHENTICATED USER'S ORDERS
	router.get("/orders" , auth.verify,  (req, res) =>
	{
		const userData = auth.decode(req.headers.authorization)
		
		userController.getOrders({id: userData.id}).then(resultFromController => res.send(resultFromController));
	});

	// RETRIEVE ALL ORDERS (ADMIN ONLY)
	router.get ("/allOrders", auth.verify, (req, res) =>
	{
		const data = auth.decode(req.headers.authorization)

		userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
	});


	

	



	



	

	






/*		------------- ALWAYS AT BOTTOM -------------		*/
// EXPORT MODULES
	module.exports = router;
	