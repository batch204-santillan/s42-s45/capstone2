/*		------------- CAPSTONE 2 -------------		*/

// DEPENDENCIES
	const express = require ("express");
	const mongoose = require("mongoose");
	const cors = require("cors");


// SERVER SETUP
	const port = 4000;
	const app = express();

// IMPORT ROUTES
	//	user
	const userRoutes = require("./routes/userRoutes");
	//	product
	const productRoutes = require("./routes/productRoutes");
	// cart
	const cartRoutes = require("./routes/cartRoutes");

// DATABASE CONNECTION
	mongoose.connect("mongodb+srv://rafsantillan:admin123@batch204-santillan.1nyzknv.mongodb.net/s42-45?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		});

		let db = mongoose.connection;
		db.on("error", () => console.error.bind(console,"Error"));
		db.once("open", () => console.log ("You are now Connected to MongoDB Atlas!"));

// MIDDLEWARES
	app.use(express.json());
	app.use(cors());


// ADDING PREFIX URL
	// users
	app.use ("/users" , userRoutes);
	// products
	app.use ("/products" , productRoutes);
	//
	app.use ("/cart" , cartRoutes);



// LISTEN
	app.listen(port, () => 
		{
			console.log (`API now online in port ${port}.`)
		});  