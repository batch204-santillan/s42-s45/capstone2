//	USER MODEL

	// DEPENDENCIES
	const mongoose = require ("mongoose");

	// SCHEMA & MODEL
	const productSchema = new mongoose.Schema(
	{	
		name:
			{
				type: String,
				required: [true, "Product name is required"]
			},
		description:
			{
				type: String,
				required: [true, "Product description is required"]
			},
		price:
			{
				type: Number,
				required: [true, "Price is required"]
			},
		isActive:
			{
				type: Boolean,
				default: true
			},
		image:
			{
				type: String,
				required:[true, "Image is required"]
			},
		
		createdOn:
			{
				type: Date,
				default: new Date()
			},
		orders:[
			{
				orderId:
					{
						type: String
					},
				nameOfCustomer: 
					{
						type: String
					},
				email:
					{
						type: String
					},
				quantity:
					{
						type: Number
					},
				totalAmount:
					{
						type: Number
					},
				placedOn:
					{
						type: Date
					}
			}
		]
	});

	// EXPORT MODEL
	module.exports = mongoose.model("Product" , productSchema)