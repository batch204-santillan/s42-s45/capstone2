//	USER MODEL

	// DEPENDENCIES
	const mongoose = require ("mongoose");

	// SCHEMA & MODEL
	const userSchema = new mongoose.Schema(
	{
		firstName:
			{
				type: String,
				required: [true, "First name is required"]
			},
		lastName:
			{
				type: String,
				required: [true, "Last Name is required"]
			},
		email:
			{
				type: String,
				required: [true, "Email is required"]
			},
		password:
			{
				type: String,
				required: [true, "Password is required"]
			},
		mobileNo:
			{
				type: String,
				required: [true, "Password is required"]
			},
		isAdmin:
			{
				type: Boolean,
				default: false
			},
		orders: [
			{
				products: 
					[{
						productId:
						{	type: String},
						name:
						{	type: String },
						price:
						{	type: Number},
						quantity:
						{	type: Number},
						subtotal:
						{	type: Number}
					}],
				
				totalAmount:
					{
						type: Number
					},
				purchasedOn:
					{
						type: Date,
						default: new Date()
					},
				orderStatus:
					{
						type: String,
						default: "Placed Order"
					}
			}
		]

	});



	// EXPORT THE MODEL
		module.exports = mongoose.model("User" , userSchema);