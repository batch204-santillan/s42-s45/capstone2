//	CART MODEL

	// DEPENDENCIES
	const mongoose = require ("mongoose");

	// SCHEMA & MODEL
	const cartSchema = new mongoose.Schema(
	{
		firstName:
			{
				type: String,
				required: [true, "First name is required"]
			},
		lastName:
			{
				type: String,
				required: [true, "Last Name is required"]
			},
		email:
			{
				type: String,
				required: [true, "Email is required"]
			},
		products:[
			{
				productId:
				{	type: String},
				productName:
				{	type: String },
				price:
				{	type: Number},
				quantity:
				{	type: Number},
				subtotal:
				{	type: Number}
			}
		],
		totalAmount:
			{
				type: Number
			}
	})


	// EXPORT THE MODEL
		module.exports = mongoose.model("Cart" , cartSchema);